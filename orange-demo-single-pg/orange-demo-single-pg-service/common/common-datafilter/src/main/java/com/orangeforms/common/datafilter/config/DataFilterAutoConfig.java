package com.orangeforms.common.datafilter.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * common-datafilter模块的自动配置引导类。
 *
 * @author Jerry
 * @date 2022-02-20
 */
@EnableConfigurationProperties({DataFilterProperties.class})
public class DataFilterAutoConfig {
}
